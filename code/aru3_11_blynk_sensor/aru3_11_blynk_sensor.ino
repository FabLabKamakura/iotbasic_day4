#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>

Adafruit_MMA8451 mma = Adafruit_MMA8451();
#define AUTH "xxxxxxxxxxxx"
#define SSID "FABLABKAMAKURA2"
#define PASS "kamakura_1192"

BLYNK_READ(V0){
  mma.setRange(MMA8451_RANGE_2_G);
  mma.read();
  int x = mma.x;
  int y = mma.y;
  int z = mma.z;
  Blynk.virtualWrite(V0, x);
  Blynk.virtualWrite(V1, y);
  Blynk.virtualWrite(V2, z);
  Serial.println(x);
  Serial.println(y);
  Serial.println(z);
}

void setup(){
  Serial.begin(9600);

  Blynk.begin(AUTH, SSID, PASS);
  if(!mma.begin()) {
    Serial.println("couldn't start");
    while(1);
  }
  Serial.println("mma8451 found");
}

void loop(){
  Blynk.run();
}

