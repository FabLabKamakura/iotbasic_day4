import mqtt.*;  // mqttライブラリをインポート
MQTTClient client;
int data;

void setup() {
  String URL = "mqtt://USERNAME:PASSWORD@HOST:PORT";
  String MQTT_TOPIC = "xxxxx";
  String MQTT_CLIENT = "iotbasic";
  size(600,600);
  background(0);
  client = new MQTTClient(this);
  client.connect(URL, MQTT_CLIENT);
  client.subscribe(MQTT_TOPIC);
  // client.unsubscribe("/example");  
}
void draw() {
  background(0);
  fill(255);
  ellipse(width/2 ,height/2 ,data/5 ,data/5);
}
void messageReceived(String topic, byte[] payload) {
  println("new message: " + topic + " - " + new String(payload));
  String t = new String(payload);
  data = Integer.parseInt(t);
}