#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <FaBoProximity_VCNL4010.h>

FaBoProximity fabo;

#define SSID "FABLABKAMAKURA2"
#define PASS "kamakura_1192"
#define MQTT_HOST   "***.cloudmqtt.com"
#define MQTT_USER   "********"
#define MQTT_PASS   "********"
int port = 17795;
#define MQTT_TOPIC  "*****" // 好きな名前をつけてください。
#define MQTT_CLIENT "*****" // 好きな名前をつけてください。

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void wifiConnect(){
  WiFi.begin(SSID, PASS);
  while(WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(500);
  }
  Serial.println("WiFi connected");
}
void setup(){
  Serial.begin(9600);
  wifiConnect();
  delay(10);
  // MQTTサーバーに接続。
  client.setServer(MQTT_HOST, port);
  if(client.connect(MQTT_CLIENT, MQTT_USER, MQTT_PASS)){
    Serial.println("connected MQTT server");
  }
  fabo.begin(); // センサー
}
void loop(){
  if(!client.connected()){
    Serial.println("failed to connect");
  }
  client.loop();

  // publisher
  if(fabo.checkAmbiReady()){
    int raw = fabo.readAmbi();
    String value = String(raw);
    const char* payload = value.c_str();
    client.publish(MQTT_TOPIC, payload);
    Serial.println(payload);
  }
  delay(10);
}


